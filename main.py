# coding=utf-8
import os

from google_speech import GoogleSpeech
from witai import WitAi
from robo import Robo
from speech_recognition.speech import SpeechRec

ACCESS_TOKEN = 'M3Z5C4M6357P4SH3VZNCSQY2CGZXWP37'
KEY = 'AIzaSyAVh6TqPmA9UPEdJh6eeFhDWNYSHanl5ZM'

file_path = os.path.join(
    os.path.dirname(__file__),
    'audios',  # pasta em que o arquivo está
    'audio5.flac')  # nome do arquivo

# transcricao do audio do microfone - google speech
speech = SpeechRec()
g_text = speech.transcreve_mic()

# deteccao da intencao da frase - wit ai
w = WitAi(ACCESS_TOKEN)
wit_text = w.get_message(g_text)
intent = wit_text['entities']['intent'][0]['value']
# print wit_text

# escolha da resposta do robo
r = Robo()
if intent == 'horas':
    r.falar_hora()

if intent == 'dia':
    r.falar_dia()
