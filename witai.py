# coding=utf-8
import requests

#ACESS_TOKEN = 'M3Z5C4M6357P4SH3VZNCSQY2CGZXWP37'


class WitAi(object):
    def __init__(self, token=None):
        if not token:
            raise ValueError('TOKEN is required, please visit https://wit.ai/home')
        self.token = token

    def get_message(self, q):
        """
        Returns the extracted meaning from a sentence, based on the app data.
        """
        url_api = 'https://api.wit.ai/message'
        if not q:
            raise ValueError('Insira um item para mensagem!')
        params = {
            'q': q,
            'access_token': self.token
        }
        result = requests.get(url_api, params=params).json()
        return result

if __name__ == "__main__":
    pass


#w = WitAi(ACESS_TOKEN)
#text = w.get_message('Acender a luz na cozinha')
#print(text)

#text = w.get_message('Tenho que tomar remédio as 19 horas?')
#print '\n\n\n'
#print text

#text = w.get_message('Qual é o seu nome?')
#print('\n\n\n',text)

#text = w.get_message('Qual cidade você está?')
#print('\n\n\n',text)

