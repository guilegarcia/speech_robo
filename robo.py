# coding=utf-8

import datetime

class Robo(object):

	def __init__(self, datahora=None):
		self.data_hora = datetime.datetime.now()

	# funcoes de resposta
	def falar_hora(self):
		print('Agora são {0} horas e {1} minutos.'.format(self.data_hora.hour, self.data_hora.minute))

	def falar_dia(self):
		print('Hoje é dia {0} do mês {1} do ano de {2}.'.format(self.data_hora.day, self.data_hora.month, self.data_hora.year))