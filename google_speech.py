# coding=utf-8

import base64
import json
import os
import requests


# arquivo de audio para transcrever
file_path = os.path.join(
os.path.dirname(__file__),
'audios', # pasta em que o arquivo está
'audio3.flac') # nome do arquivo

# EX: https://progfruits.wordpress.com/2014/05/31/using-google-speech-api-from-python/

class GoogleSpeech(object):
    # URL https://speech.googleapis.com/v1beta1/speech:syncrecognize?key=$API_KEY

    def __init__(self, key=None):
        if not key:
            raise ValueError('KEY is required, please visit Google API')
        self.key = key

    def get_audio(self, file_path):
        """
        Recebe o caminho do arquivo e retorna o arquivo de áudio
        """
        # Referências base64 encoding #
        #   - "Embedding audio content" em https://cloud.google.com/speech/docs/basics
        #   - "Embedding audio content programmatically" em https://cloud.google.com/speech/docs/base64-encoding

        # loads audio into memory
        with open(file_path, 'rb') as audio_file:
            content = base64.b64encode(audio_file.read())
            return content 
            # retorna o arquivo de audio base64 encoded

    def get_text_from_audio(self, audio):
        """
        Recebe o áudio e retorna o texto transcrito
        """
        if not audio:
            raise ValueError('Insira um áudio!')

        url_api = 'https://speech.googleapis.com/v1beta1/speech:syncrecognize'

        # params do arquivo de áudio enviado
        data = {
            # Referências #
            # passar por parametros o 'config', 'audio' e 'key': https://cloud.google.com/speech/reference/rest/v1beta1/speech/asyncrecognize
            # 'config' e 'audio' são json. Como passar json por requests http://stackoverflow.com/a/41772382
            'config': {
                "encoding": "FLAC",
                "sampleRate": 44100,
                "languageCode": "pt-BR",
            },
            'audio': {
                'content': audio.encode('UTF-8')
            },
        }

        params = {
            'key': 'AIzaSyAVh6TqPmA9UPEdJh6eeFhDWNYSHanl5ZM'
        }
            # passa o data do audio como arquivo json e a key como param pelo método POST no request
        result = requests.post(url_api, params=params, json=data).json()
        return result


    def get_from_json(self, arquivo_json):
        #imprime apenas o texto extraído do aúdio
        try:
            transcript = arquivo_json['results'][0]['alternatives'][0]['transcript']
        except KeyError:
            print "Ops! Arquivo JSON inválido, áudio não detectado."
        else:
            return transcript
            
if __name__ == "__main__":
    pass


#google_speech = GoogleSpeech(key='AIzaSyAVh6TqPmA9UPEdJh6eeFhDWNYSHanl5ZM')

#audio1 = google_speech.get_audio(file_path)

#arquivo_json = google_speech.get_text_from_audio(audio1)
#print arquivo_json

#texto = google_speech.get_from_json(arquivo_json)
#print texto
