# coding=utf-8
import speech_recognition as sr


# import snowboydecoder


# def detected_callback():
#     print "hotword detected"

# detector = snowboydecoder.HotwordDetector("ola_robo.pmdl", sensitivity=0.5, audio_gain=1)
# detector.start(detected_callback)

# recognizer = sr.Recognizer()
# microphone = sr.Microphone()

class SpeechRec(object):
    recognizer = sr.Recognizer()
    microphone = sr.Microphone()

    def transcreve_mic(self):

        with self.microphone as source:
            print('Diga algo... ')
            audio = self.recognizer.listen(source)

        try:
            transcrito = self.recognizer.recognize_google(audio).encode('utf-8')
            print('Google Speech Recognition: {0}'.format(transcrito))
            return transcrito
        except sr.UnknownValueError:
            print('Audio nao reconhecido pelo GoogleSpeech')
        except sr.RequestError as e:
            print('Nao se pode fazer o pedido ao GoogleSpeech')

# speechRec = SpeechRec()
# speechRec.transcreve_mic()
